### Web - Create TestNG test and run it

The project was created based on below instructions:

Open browser (any browser that you want)\
Go to https://opensource-demo.orangehrmlive.com/  \
Login with credentials appears on site\
Go to "Time" in left options bar\
Under section of "Timesheets Pending Action" section, click view for “Timesheet period” of 2022-08-15 - 2022-08-21\
Click on the green comment button in the first row\
Print the text in comment and compare to text – “Leadership Development”\
Think about negative test and implement it
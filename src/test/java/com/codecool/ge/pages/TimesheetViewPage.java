package com.codecool.ge.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TimesheetViewPage extends BasePage {
    private final String timesheetViewUrl = baseUrl + "web/index.php/time/viewTimesheet/employeeId/7?startDate=2022-08-15";

    @FindBy(css = ".oxd-userdropdown-tab")
    WebElement userDropdown;

    @FindBy(linkText = "Logout")
    WebElement logoutLink;

    @FindBy(css = ".oxd-icon.bi-chat-dots-fill")
    WebElement commentOpenButton;

    @FindBy(css = "textarea[placeholder='Comment here']")
    WebElement commentArea;

    @FindBy(css = ".oxd-dialog-close-button.oxd-dialog-close-button-position")
    WebElement commentCloseButton;

    @FindBy(css = ".oxd-text.oxd-text--p.orangehrm-timeperiod-title")
    WebElement timePeriodTitle;

    public void logout() {
        wait.until(ExpectedConditions.visibilityOf(userDropdown));
        userDropdown.click();
        logoutLink.click();
    }

    private void openCommentModal() {
        commentOpenButton.click();
    }

    private void closeCommentModal() {
        commentCloseButton.click();
    }

    public String getCommentText() {
        wait.until(ExpectedConditions.visibilityOf(commentOpenButton));
        openCommentModal();
        wait.until(ExpectedConditions.attributeToBeNotEmpty(commentArea, "value"));
        String comment = commentArea.getAttribute("value");
        closeCommentModal();
        return comment;
    }

    public void openViewPage() {
        webDriver.get(timesheetViewUrl);
    }

    public boolean commentButtonNotPresent() {
        return ExpectedConditions.invisibilityOf(commentOpenButton).apply(webDriver);
    }

    public boolean timesheetPresent() {
        wait.until(ExpectedConditions.visibilityOf(timePeriodTitle));
        return timePeriodTitle.isDisplayed();
    }

}

package com.codecool.ge.pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebdriverUtil {

    private static final int SECONDS = 10;
    private WebDriver webDriver;
    private final WebDriverWait webDriverWait;
    private static WebdriverUtil INSTANCE;

    private WebdriverUtil() {
        webDriver = setupWebdriver();
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(SECONDS));
    }

    public static WebdriverUtil getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WebdriverUtil();
        }
        return INSTANCE;
    }

    public WebDriver getWebDriver() {
        return this.webDriver;
    }

    public WebDriverWait getWebDriverWait() {
        return webDriverWait;
    }

    private WebDriver setupWebdriver() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions options=new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1920,1080");
        webDriver = new ChromeDriver(options);
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        return webDriver;
    }

    public void tearDown() {
        webDriver.quit();
        INSTANCE = null;
    }
}

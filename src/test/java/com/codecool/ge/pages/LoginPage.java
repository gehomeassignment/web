package com.codecool.ge.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends BasePage {

    private final String loginUrl = baseUrl;

    @FindBy(css = "input[placeholder='Username']")
    WebElement usernameInput;

    @FindBy(css = "div[class='orangehrm-login-error'] p:nth-child(1)")
    WebElement usernameField;

    @FindBy(css = "input[placeholder='Password']")
    WebElement passwordInput;

    @FindBy(css = "div[class='orangehrm-login-error'] p:nth-child(2)")
    WebElement passwordField;

    @FindBy(css = "button[type='submit']")
    WebElement loginButton;

    private String readOutText(WebElement field) {
        String[] text = field.getText().split(":");
        return text[1].trim();
    }

    public void openMainPage() {
        webDriver.get(loginUrl);
        wait.until(ExpectedConditions.visibilityOf(usernameInput));
    }

    public void login() {
        usernameInput.sendKeys(readOutText(usernameField));
        passwordInput.sendKeys(readOutText(passwordField));
        loginButton.submit();
    }

    public boolean isLoginDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(usernameInput));
        return usernameInput.isDisplayed();
    }

}

package com.codecool.ge.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TimePage extends BasePage {

    @FindBy(xpath = "//div[text()='2022-08-15 - 2022-08-21']//ancestor::div[1]//following-sibling::div//child::div//child::button")
    WebElement definedViewButton;

    @FindBy(xpath = "(//button[@type='button'][normalize-space()='View'])[1]")
    WebElement firstViewButton;

    public void navigateToTimesheetView() {
        wait.until(ExpectedConditions.elementToBeClickable(definedViewButton));
        definedViewButton.click();
    }

    public void navigateToSheetNoComment() {
        wait.until(ExpectedConditions.elementToBeClickable(firstViewButton));
        firstViewButton.click();
    }
}

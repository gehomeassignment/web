package com.codecool.ge.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DashboardPage extends BasePage {

    @FindBy(linkText = "Time")
    WebElement timeLink;

    public void navigateToTimePage() {
        wait.until(ExpectedConditions.visibilityOf(timeLink));
        timeLink.click();
    }
}

package com.codecool.ge.tests;

import com.codecool.ge.pages.DashboardPage;
import com.codecool.ge.pages.LoginPage;
import com.codecool.ge.pages.TimePage;
import com.codecool.ge.pages.TimesheetViewPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;


public class TimesheetCommentTest {

    private LoginPage loginPage;
    private DashboardPage dashboardPage;
    private TimePage timePage;
    private TimesheetViewPage timesheetViewPage;
    private boolean shouldLogout = true;

    @BeforeMethod
    public void setUp() {
        loginPage = new LoginPage();
        dashboardPage = new DashboardPage();
        timePage = new TimePage();
        timesheetViewPage = new TimesheetViewPage();
    }

    @AfterMethod
    public void tearDown() {
        if (shouldLogout) {
            timesheetViewPage.logout();
        }
        loginPage.closeWebDriver();
    }

    @Test(description = "Get comment text successfully after login, navigate to timesheet and open comment modal")
    public void checkComment() {
        loginPage.openMainPage();
        loginPage.login();
        dashboardPage.navigateToTimePage();
        timePage.navigateToTimesheetView();
        String commentText = timesheetViewPage.getCommentText();
        System.out.println("Comment:");
        System.out.println(commentText);
        Assert.assertEquals(commentText, "Leadership Development");
    }

    @Test(description = "Comment button not present and login page displayed, when timesheet page opened without login")
    public void checkCommentWoLogin() {
        shouldLogout = false;
        timesheetViewPage.openViewPage();
        Assert.assertTrue(timesheetViewPage.commentButtonNotPresent());
        Assert.assertTrue(loginPage.isLoginDisplayed());
    }

    @Test(description = "Comment button not present, when timesheet without comment opened")
    public void checkCommentDifferentTimesheet() {
        loginPage.openMainPage();
        loginPage.login();
        dashboardPage.navigateToTimePage();
        timePage.navigateToSheetNoComment();
        Assert.assertTrue(timesheetViewPage.timesheetPresent());
        Assert.assertTrue(timesheetViewPage.commentButtonNotPresent());
    }
}
